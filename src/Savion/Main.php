<?php
        /*   _       _________ _______  _______  _______ 
|\     /||\     /|( (    /|\__   __/(  ____ \(  ____ )(  ____ \
| )   ( || )   ( ||  \  ( |   ) (   | (    \/| (    )|| (    \/
| (___) || |   | ||   \ | |   | |   | (__    | (____)|| (_____ 
|  ___  || |   | || (\ \) |   | |   |  __)   |     __)(_____  )
| (   ) || |   | || | \   |   | |   | (      | (\ (         ) |
| )   ( || (___) || )  \  |   | |   | (____/\| ) \ \__/\____) |
|/     \|(_______)|/    )_)   )_(   (_______/|/   \__/\_______)
                                                               
                                                               */


namespace Savion;

use pocketmine\utils\Config;
use pocketmine\utils\TextFormat;

use pocketmine\Player;
use pocketmine\Server;

use pocketmine\event\Listener;

use pocketmine\plugin\PluginBase;

use pocketmine\command\Command;
use pocketmine\command\CommandSender;
use pocketmine\command\ConsoleCommandSender;


use pocketmine\scheduler\PluginTask;
use pocketmine\scheduler\CallbackTask;

use pocketmine\event\entity\EntityDeathEvent;
use pocketmine\event\entity\EntityDamageEvent;

use pocketmine\math\Vector3;

use pocketmine\block\Block;

use pocketmine\tile\Sign;
use pocketmine\tile\Tile;

use pocketmine\level\Position;
use pocketmine\level\Level;

use pocketmine\event\player\PlayerInteractEvent;


class Main extends PluginBase implements Listener
{

public function onLoad()
{
	
$this->setGame1 = array();
$this->setGame2 = array();
$this->setGame3 = array();
$this->setGame4 = array();

$this->setRoom1 = array();
$this->setRoom2 = array();
$this->setRoom3 = array();
$this->setRoom4 = array();

$this->Setter = array();

$this->players1 = array(); //game1 players
$this->players2 = array(); //game2 players
$this->players3 = array(); //game3 players
$this->players4 = array(); //game4 players

$this->status1 = 0;
$this->status2 = 0;
$this->status3 = 0;
$this->status4 = 0;

$this->g1time = 0;
$this->g2time = 0;
$this->g3time = 0;
$this->g4time = 0;

$this->all = 10;

$this->getServer()->getLogger()->info("[Hunters]Loading....");
}

	       public function onEnable()
	       {
	    	@mkdir($this->getDataFolder());
			
		$this->config=new Config($this->getDataFolder() . "config.yml", Config::YAML, array(
		"playTime" => 180,
		"queueTime" => 60,
		"money" => 50));
		
		$this->config->save();
		$this->getServer()->getPluginManager()->registerEvents($this,$this);	
		$this->getServer()->getLogger()->info("[Hunters]Loaded!");
		
		$this->getServer()->getScheduler()->scheduleRepeatingTask(new gameTask($this),20); 

		
		$this->w1=$this->config->get("[game1]waitroom");
		$this->w2=$this->config->get("[game2]waitroom");
		$this->w3=$this->config->get("[game3]waitroom");
		$this->w4=$this->config->get("[game4]waitroom");
		
		$this->waitroom1=new Vector3($this->w1["x"],$this->w1["y"],$this->w1["z"]);
		$this->waitroom2=new Vector3($this->w2["x"],$this->w2["y"],$this->w2["z"]);
		$this->waitroom3=new Vector3($this->w3["x"],$this->w3["y"],$this->w3["z"]);
		$this->waitroom4=new Vector3($this->w4["x"],$this->w4["y"],$this->w4["z"]);
		
		$this->s1=$this->config->get("[game1]sign");
		$this->s2=$this->config->get("[game2]sign");
		$this->s3=$this->config->get("[game3]sign");
		$this->s4=$this->config->get("[game4]sign");
		
		$this->g1sign=new Vector3($this->s1["x"],$this->s1["y"],$this->s1["z"]);
		$this->g2sign=new Vector3($this->s2["x"],$this->s2["y"],$this->s2["z"]);
		$this->g3sign=new Vector3($this->s3["x"],$this->s3["y"],$this->s3["z"]);
		$this->g4sign=new Vector3($this->s4["x"],$this->s4["y"],$this->s4["z"]);
		
		$this->g1p1=$this->config->get("[game1]pos1");
		$this->g1p2=$this->config->get("[game1]pos2");
		
		$this->g2p1=$this->config->get("[game2]pos1");
		$this->g2p2=$this->config->get("[game2]pos2");
		
		$this->g3p1=$this->config->get("[game3]pos1");
		$this->g3p2=$this->config->get("[game3]pos2");
		
		$this->g4p1=$this->config->get("[game4]pos1");
		$this->g4p2=$this->config->get("[game4]pos2");
		
		$this->g1pos1=new Vector3($this->g1p1["x"],$this->g1p1["y"],$this->g1p1["z"]);
		$this->g1pos2=new Vector3($this->g1p2["x"],$this->g1p2["y"],$this->g1p2["z"]);
		
		$this->g2pos1=new Vector3($this->g2p1["x"],$this->g2p1["y"],$this->g2p1["z"]);
		$this->g2pos2=new Vector3($this->g2p2["x"],$this->g2p2["y"],$this->g2p2["z"]);
		
		$this->g3pos1=new Vector3($this->g3p1["x"],$this->g3p1["y"],$this->g3p1["z"]);
		$this->g3pos2=new Vector3($this->g3p2["x"],$this->g3p2["y"],$this->g3p2["z"]);
		
		$this->g4pos1=new Vector3($this->g4p1["x"],$this->g4p1["y"],$this->g4p1["z"]);
		$this->g4pos2=new Vector3($this->g4p2["x"],$this->g4p2["y"],$this->g4p2["z"]);
	
		
		}
		
		public function onCommand(CommandSender $sender, Command $command, $label, array $args)
	        {
		if(!isset($args[0])){
			unset($sender,$cmd,$label,$args);
			return false;
			
		};
		switch ($args[0])
		{
			case "setgame":
	if(!$this->config->exists("game1") && !$this->config->exists("game4") && !$this->config->exists("game2") && !$this->config->exists("game3")){
	
		 $this->set1($sender);
		$sender->sendMessage("Please tap a sign!");
		return true;
		}
		
		if(!$this->config->exists("game2") && $this->config->exists("game1")){
		$this->set2($sender);
		$sender->sendMessage("Please tap a sign!"); 
		return true;
		}
		
		if(!$this->config->exists("game3") && $this->config->exists("game2") && $this->config->exists("game1")){
		$this->set3($sender);
		$sender->sendMessage("Please tap a sign!"); 
		return true;
		}
		
		if(!$this->config->exists("game4") && $this->config->exists("game2") && $this->config->exists("game3") && $this->config->exists("game1")){
		$this->set4($sender);
		$sender->sendMessage("Please tap a sign!"); 
		return true;
		}
		
		if($this->config->exists("game4") && $this->config->exists("game2") && $this->config->exists("game3") && $this->config->exists("game1")){
			
   		$sender->sendMessage("Max game count has already been set!");
		return;
		}
		break;
		
		
		case "setroom":
		if(!$this->config->exists("[game1]waitroom") && !$this->config->exists("[game4]waitroom") && !$this->config->exists("[game2]waitroom") && !$this->config->exists("[game3]waitroom")){
		 $this->setRoom1($sender);
		$sender->sendMessage("Please tap a redstone block!"); 
		return true;
		}
		
		if(!$this->config->exists("[game2]waitroom") && $this->config->exists("[game1]waitroom")){
		$this->setRoom2($sender);
		$sender->sendMessage("Please tap a redstone block!"); 
		return true;
		}
		
		if(!$this->config->exists("[game3]waitroom") && $this->config->exists("[game2]waitroom") && $this->config->exists("[game1]waitroom")){
		$this->setRoom3($sender);
	        $sender->sendMessage("Please tap a redstone block!"); 
		return true;
		}
		
		if(!$this->config->exists("[game4]waitroom") && $this->config->exists("[game2]waitroom") && $this->config->exists("[game3]waitroom") && $this->config->exists("[game1]waitroom")){
		$this->setRoom4($sender);
		$sender->sendMessage("Please tap a redstone block!"); 
		return true;
		}
		
		if($this->config->exists("[game1]waitroom") && $this->config->exists("[game2]waitroom") && $this->config->exists("[game3]waitroom") && $this->config->exists("[game4]waitroom")){
			
   		$sender->sendMessage("Max waitrooms have been set!");
		return;
		}
		break;
		}
	        }
		
		//add gametask
		
		public function room1(PlayerInteractEvent $ev){
		$p = $ev->getPlayer();
		$block = $ev->getBlock();
		$levelname = $p->getLevel()->getName();
		if(isset($this->setRoom1[$p->getName()]) && $p->isOp() === true){
		switch($this->Setter[$p->getName()]){
		case 0:
		        // Got lazy putting down TextFormat blah blah... -CavinMiana
			if($ev->getBlock()->getID() != 152){
				$p->sendMessage("Please tap a REDSTONE BLOCK!");
					return;
				}
				
					$this->waitroom=array(
					"x" =>$block->getX(),
					"y" =>$block->getY(),
					"z" =>$block->getZ(),
					"level" =>$levelname);
					
						unset($this->Setter[$p->getName()]);
				unset($this->setRoom1[$p->getName()]);
				$this->config->set("[game1]waitroom",$this->waitroom);
				$this->config->save();
				$p->sendMessage("Waitroom created!");
				break;
		}
	}
}

public function room2(PlayerInteractEvent $ev){
		$p = $ev->getPlayer();
		$block = $ev->getBlock();
		$levelname = $p->getLevel()->getName();
		if(isset($this->setRoom2[$p->getName()]) && $p->isOp() === true){
		switch($this->Setter[$p->getName()]){
		case 0:
		
			if($ev->getBlock()->getID() != 152){
				$p->sendMessage("Please tap a REDSTONE BLOCK!"); 
					return;
				}
				
					$this->waitroom=array(
					"x" =>$block->getX(),
					"y" =>$block->getY(),
					"z" =>$block->getZ(),
					"level" =>$levelname);
						unset($this->Setter[$p->getName()]);
				unset($this->setRoom1[$p->getName()]);
				$this->config->set("[game2]waitroom",$this->waitroom);
				$this->config->save();
				$p->sendMessage("Waitroom created!");
				break;
		}
	}
}

public function room3(PlayerInteractEvent $ev){
		$p = $ev->getPlayer();
		$block = $ev->getBlock();
		$levelname = $p->getLevel()->getName();
		if(isset($this->setRoom3[$p->getName()]) && $p->isOp() === true){
		switch($this->Setter[$p->getName()]){
		case 0:
		
			if($ev->getBlock()->getID() != 152){
				$p->sendMessage("Please tap a REDSTONE BLOCK!"); 
					return;
				}
				
					$this->waitroom=array(
					"x" =>$block->getX(),
					"y" =>$block->getY(),
					"z" =>$block->getZ(),
					"level" =>$levelname);
						unset($this->Setter[$p->getName()]);
				unset($this->setRoom3[$p->getName()]);
				$this->config->set("[game3]waitroom",$this->waitroom);
				$this->config->save();
				$p->sendMessage("Waitroom created!");
				break;
		}
	}
}

public function room4(PlayerInteractEvent $ev){
		$p = $ev->getPlayer();
		$block = $ev->getBlock();
		$levelname = $p->getLevel()->getName();
		if(isset($this->setRoom4[$p->getName()]) && $p->isOp() === true){
		switch($this->Setter[$p->getName()]){
		case 0:
		
			if($ev->getBlock()->getID() != 152){
				$p->sendMessage("Please tap a REDSTONE BLOCK!"); 
					return;
				}
				
					$this->waitroom=array(
					"x" =>$block->getX(),
					"y" =>$block->getY(),
					"z" =>$block->getZ(),
					"level" =>$levelname);
						unset($this->Setter[$p->getName()]);
				unset($this->setRoom4[$p->getName()]);
				$this->config->set("[game4]waitroom",$this->waitroom);
				$this->config->save();
				$p->sendMessage("Waitroom created!");
				break;
		}
	}
}

public function setGame4(PlayerInteractEvent $ev){
         	$p = $ev->getPlayer();
		$block = $ev->getBlock();
		$levelname = $p->getLevel()->getName();
		if(isset($this->setGame4[$p->getName()]) && $p->isOp() === true){
		switch($this->Setter[$p->getName()]){
		case 0:
		
			if($ev->getBlock()->getID() != 63 && $ev->getBlock()->getID() != 68){
				$p->sendMessage("Please tap a SIGN!"); 
					return;
				}
				
	                	$this->sign=array(
					"x" =>$block->getX(),
					"y" =>$block->getY(),
					"z" =>$block->getZ(),
					"level" =>$levelname,
					"sign");
					$this->Setter[$p->getName()]++;
				$this->config->set("[game4]sign",$this->sign);
				$this->config->save();
				$p->sendMessage("Sign created!");
				break;
				
				/*Hunters spot*/ case 1:
				$this->pos1=array(
					"x" =>$block->x,
					"y" =>$block->y, 
					"z" =>$block->z,
					"level" =>$levelname,
					"pos1");
					$this->Setter[$p->getName()]++;
				$this->config->set("[game4]pos1",$this->pos1);
				$this->config->save();
				$p->sendMessage("Pos1 created!");
				break;
				
				/*Hunters spot*/case 2:
				$this->pos2=array(
					"x" =>$block->x,
					"y" =>$block->y,
					"z" =>$block->z,
					"level" =>$levelname,
					"pos2"); 
				$this->config->set("[game4]pos2",$this->pos2);
				$this->config->set("game4","true");
				unset($this->pos1);
				unset($this->pos2);
				unset($this->Setter[$p->getName()]);
				unset($this->setGame4[$p->getName()]);
				$this->config->save();
				$p->sendMessage("Pos2 created! everything setup(reminder: remember todo /h setroom to set the waitroom");
				break;
				
		}
		
		}else{

$sign = $p->getLevel()->getTile($block);
/* will this work? */if($sign instanceof Vector3 && $this->config->exists("[game4]sign") && $sign->getX() === $this->config->get("[game4]sign")["x"] && $sign->getY() === $this->config->get("[game4]sign")["y"] && $sign->getZ() === $this->config->get("[game4]sign")["z"] && $sign instanceof Sign && $p->getLevel()->getName() === $this->config->get("[game4]sign")["level"] && $this->config->exists("game4") && !isset($this->players4[$p->getName()])){ 
		$this->addGamePlayer4($p);
		if(!$this->config->exists("[game4]waitroom")){
		$p->sendMessage("Waitroom isnt setup!");
		return;
		}
		
		$p->setLevel($this->getServer()->getLevelByName($this->config->get("[game4]waitroom")["level"]));
		$p->teleport($this->config->get("[game4]waitroom"));
		
		foreach($this->players4 as $pl){
		$pl->sendMessage($p->getName()." Joined the match!");
		
		}
		//more todo
		}
		}
		
		}

public function setGame3(PlayerInteractEvent $ev){
         	$p = $ev->getPlayer();
		$block = $ev->getBlock();
		$levelname = $p->getLevel()->getName();
		if(isset($this->setGame3[$p->getName()]) && $p->isOp() === true){
		switch($this->Setter[$p->getName()]){
		case 0:
		
			if($ev->getBlock()->getID() != 63 && $ev->getBlock()->getID() != 68){
				$p->sendMessage("Please tap a SIGN!"); 
					return;
				}
				
	                	$this->sign=array(
					"x" =>$block->getX(),
					"y" =>$block->getY(),
					"z" =>$block->getZ(),
					"level" =>$levelname,
					"sign");
					$this->Setter[$p->getName()]++;
				$this->config->set("[game3]sign",$this->sign);
				$this->config->save();
				$p->sendMessage("Sign created!");
				break;
				
				/*Hunters spot*/ case 1:
				$this->pos1=array(
					"x" =>$block->x,
					"y" =>$block->y, 
					"z" =>$block->z,
					"level" =>$levelname,
					"pos1");
					$this->Setter[$p->getName()]++;
				$this->config->set("[game3]pos1",$this->pos1);
				$this->config->save();
				$p->sendMessage("Pos1 created!");
				break;
				
				/*Hunters spot*/case 2:
				$this->pos2=array(
					"x" =>$block->x,
					"y" =>$block->y,
					"z" =>$block->z,
					"level" =>$levelname,
					"pos2"); 
				$this->config->set("[game3]pos2",$this->pos2);
				$this->config->set("game3","true");
				unset($this->pos1);
				unset($this->pos2);
				unset($this->Setter[$p->getName()]);
				unset($this->setGame3[$p->getName()]);
				$this->config->save();
				$p->sendMessage("Pos2 created! everything setup(reminder: remember todo /h setroom to set the waitroom");
				break;
                                // Stopped right here with the Color Format stuff. I will continue tommorow or tonight -CavinMiana
                                // Fixed Capitalization, Spelling, and added Color. -CavinMiana.
				
		}
		
		}else{
			
		$sign = $p->getLevel()->getTile($block);
/* will this work? */if($sign instanceof Vector3 && $this->config->exists("[game3]sign") && $sign->getX() === $this->config->get("[game3]sign")["x"] && $sign->getY() === $this->config->get("[game3]sign")["y"] && $sign->getZ() === $this->config->get("[game3]sign")["z"] && $sign instanceof Sign && $p->getLevel()->getName() === $this->config->get("[game3]sign")["level"] && $this->config->exists("game3") && !isset($this->players3[$p->getName()])){
		$this->addGamePlayer3($p);
		if(!$this->config->exists("[game3]waitroom")){
		$p->sendMessage("Waitroom isnt setup!");
		return;
		}
		
		$p->setLevel($this->getServer()->getLevelByName($this->config->get("[game3]waitroom")["level"]));
		$p->teleport($this->config->get("[game3]waitroom"));
		
		foreach($this->players3 as $pl){
		$pl->sendMessage($p->getName()." Joined the match!");
		
		}
		//more todo
		}
		}
		
		}


public function setGame2(PlayerInteractEvent $ev){
         	$p = $ev->getPlayer();
		$block = $ev->getBlock();
		$levelname = $p->getLevel()->getName();
		if(isset($this->setGame2[$p->getName()]) && $p->isOp() === true){
		switch($this->Setter[$p->getName()]){
		case 0:
		
			if($ev->getBlock()->getID() != 63 && $ev->getBlock()->getID() != 68){
				$p->sendMessage("please tap a SIGN!"); 
					return;
				}
				
	                	$this->sign=array(
					"x" =>$block->getX(),
					"y" =>$block->getY(),
					"z" =>$block->getZ(),
					"level" =>$levelname,
					"sign");
					$this->Setter[$p->getName()]++;
				$this->config->set("[game2]sign",$this->sign);
				$this->config->save();
				$p->sendMessage("sign created!");
				break;
				
				/*Hunters spot*/ case 1:
				$this->pos1=array(
					"x" =>$block->x,
					"y" =>$block->y, 
					"z" =>$block->z,
					"level" =>$levelname,
					"pos1");
					$this->Setter[$p->getName()]++;
				$this->config->set("[game2]pos1",$this->pos1);
				$this->config->save();
				$p->sendMessage("pos1 created!");
				break;
				
				/*Hunters spot*/case 2:
				$this->pos2=array(
					"x" =>$block->x,
					"y" =>$block->y,
					"z" =>$block->z,
					"level" =>$levelname,
					"pos2"); 
				$this->config->set("[game2]pos2",$this->pos2);
				$this->config->set("game2","true");
				unset($this->pos1);
				unset($this->pos2);
				unset($this->Setter[$p->getName()]);
				unset($this->setGame2[$p->getName()]);
				$this->config->save();
				$p->sendMessage("pos2 created! everything setup(reminder: remember todo /h setroom to set the waitroom");
				break;
				
		}
		
		}else{

$sign = $p->getLevel()->getTile($block);
/* will this work? */if($sign instanceof Vector3 && $this->config->exists("[game2]sign") && $sign->getX() === $this->config->get("[game2]sign")["x"] && $sign->getY() === $this->config->get("[game2]sign")["y"] && $sign->getZ() === $this->config->get("[game2]sign")["z"] && $sign instanceof Sign && $p->getLevel()->getName() === $this->config->get("[game2]sign")["level"] && $this->config->exists("game2") && !isset($this->players2[$p->getName()])){
		$this->addGamePlayer2($p);
		if(!$this->config->exists("[game2]waitroom")){
		$p->sendMessage("Waitroom isnt setup!");
		return;
		}
		
		$p->setLevel($this->getServer()->getLevelByName($this->config->get("[game2]waitroom")["level"]));
		$p->teleport($this->config->get("[game2]waitroom"));
		
		foreach($this->players2 as $pl){
		$pl->sendMessage($p->getName()." Joined the match!");
		
		}
		//more todo
		}
		}
		
		}
		
		
		public function setGame1(PlayerInteractEvent $ev){
         	$p = $ev->getPlayer();
		$block = $ev->getBlock();
		$levelname = $p->getLevel()->getName();
		if(isset($this->setGame1[$p->getName()]) && $p->isOp() === true){
		switch($this->Setter[$p->getName()]){
		case 0:
		
			if($ev->getBlock()->getID() != 63 && $ev->getBlock()->getID() != 68){
				$p->sendMessage("please tap a SIGN!"); 
					return;
				}
				
	                	$this->sign=array(
					"x" =>$block->getX(),
					"y" =>$block->getY(),
					"z" =>$block->getZ(),
					"level" =>$levelname,
					"sign");
					$this->Setter[$p->getName()]++;
				$this->config->set("[game1]sign",$this->sign);
				$this->config->save();
				$p->sendMessage("sign created!");
				break;
				
				/*Hunters spot*/ case 1:
				$this->pos1=array(
					"x" =>$block->x,
					"y" =>$block->y, 
					"z" =>$block->z,
					"level" =>$levelname,
					"pos1");
					$this->Setter[$p->getName()]++;
				$this->config->set("[game1]pos1",$this->pos1);
				$this->config->save();
				$p->sendMessage("pos1 created!");
				break;
				
				/*Hunters spot*/case 2:
				$this->pos2=array(
					"x" =>$block->x,
					"y" =>$block->y,
					"z" =>$block->z,
					"level" =>$levelname,
					"pos2"); 
				$this->config->set("[game1]pos2",$this->pos2);
				$this->config->set("game1","true");
				unset($this->pos1);
				unset($this->pos2);
				unset($this->Setter[$p->getName()]);
				unset($this->setGame1[$p->getName()]);
				$this->config->save();
				$p->sendMessage("pos2 created! everything setup(reminder: remember todo /h setroom to set the waitroom");
				break;
				
				
		
		}
		 
		}else{
			
		$sign = $p->getLevel()->getTile($block);
/* will this work? */if($sign instanceof Vector3 && $this->config->exists("[game1]sign") && $sign->getX() === $this->config->get("[game1]sign")["x"] && $sign->getY() === $this->config->get("[game1]sign")["y"] && $sign->getZ() === $this->config->get("[game1]sign")["z"] && $sign instanceof Sign && $p->getLevel()->getName() === $this->config->get("[game1]sign")["level"] && $this->config->exists("game1") && !isset($this->players1[$p->getName()])){
		$this->addGamePlayer1($p);
		if(!$this->config->exists("[game1]waitroom")){
		$p->sendMessage("Waitroom isnt setup!");
		return;
		}
		
		$p->setLevel($this->getServer()->getLevelByName($this->config->get("[game1]waitroom")["level"]));
		$p->teleport($this->waitroom1);
		 
		 foreach($this->players1 as $key=>$val)
{
$pl = $this->getServer()->getPlayer($val["Player"]);
		
		$pl->sendMessage($p->getName()." Joined the match!");
		
		}
		//more todo
		}
		}
		
		}
		
		//add kills?
		public function setRoom1(Player $p){ 
				unset($this->Setter[$p->getName()]);
				unset($this->setRoom1[$p->getName()]);
	        $this->setRoom1[$p->getName()] = array("Player" => $p->getName());
		$this->Setter[$p->getName()] = 0;
		}
		
		public function setRoom2(Player $p){ 
				unset($this->Setter[$p->getName()]);
				unset($this->setRoom2[$p->getName()]);
	        $this->setRoom2[$p->getName()] = array("Player" => $p->getName());
		$this->Setter[$p->getName()] = 0;
		}
		
		public function setRoom3(Player $p){ 
				unset($this->Setter[$p->getName()]);
				unset($this->setRoom3[$p->getName()]);
	        $this->setRoom3[$p->getName()] = array("Player" => $p->getName());
		$this->Setter[$p->getName()] = 0;
		}
		
		public function setRoom4(Player $p){ 
				unset($this->Setter[$p->getName()]);
				unset($this->setRoom4[$p->getName()]);
	        $this->setRoom4[$p->getName()] = array("Player" => $p->getName());
		$this->Setter[$p->getName()] = 0;
		}
		
		public function set1(Player $p){ 
		unset($this->Setter[$p->getName()]);
	        $this->setGame1[$p->getName()] = array("Player" => $p->getName());
		$this->Setter[$p->getName()] = 0;
		}
		
		public function set2(Player $p){
		unset($this->Setter[$p->getName()]);
        	$this->setGame2[$p->getName()] = array("Player" => $p->getName());
		$this->Setter[$p->getName()] = 0;
		}
		
		public function set3(Player $p){
		unset($this->Setter[$p->getName()]);
		$this->setGame3[$p->getName()] = array("Player" => $p->getName());	
		$this->Setter[$p->getName()] = 0;
		}
		
		public function set4(Player $p){
		unset($this->Setter[$p->getName()]);
		$this->setGame4[$p->getName()] = array("Player" => $p->getName());
		$this->Setter[$p->getName()] = 0;
		}	
		
	public function addGamePlayer1(Player $p){
		$this->players1[$p->getName()] = array("Player" => $p->getName());
	}
	
	public function addGamePlayer2(Player $p){
		$this->players2[$p->getName()] = array("Player" => $p->getName());
	}
	
	public function addGamePlayer3(Player $p){
		$this->players3[$p->getName()] = array("Player" => $p->getName());
	}
	
	public function addGamePlayer4(Player $p){
		$this->players4[$p->getName()] = array("Player" => $p->getName());
	}
	
		
		
		
		
		
		 
		public function game2(){
			foreach($this->players2 as $p){
			$this->g2time=0;
			$this->g2time=$this->config->get("queueTime");
			if($this->status2 === 1){
				switch($this->g2time){
						
						case 5:
						$p->sendTip("Game starts in {$this->g2time} seconds!");
						break;
						
						case 60:
						$p->sendMessage("Game starts in 1 minute!");
						break;
						
						case 0:
						$p->sendMessage("Game has started!");
						$pl = $this->players2;
						$vip = array_rand($pl,1);
	                    $vip->sendMessage("You are the VIP!");
						$vip->teleport($this->g2p1);
						$p->teleport($this->g2p2);
						$this->g2time=0;
						$this->g2time=$this->config->get("playTime");
						$this->status2=2;
						break;
					}
				}
					
					
				
			if($this->status2 === 2){
			switch($this->g2time){
			
				case 5:
						$p->sendTip("Game ends in {$this->g2time} seconds!");
						break;
						
						case 60:
						$p->sendMessage("Game ends in 1 minute!");
						break;
						
						case 120:
						$p->sendMessage("Game ends in 2 minutes");
						break;
						
						case 180:
						$p->sendMessage("Game ends in 3 minutes!");
						break;
						
						case 0:
						$p->sendMessage("Game has ended!");
						$p->setLevel($this->getServer()->getLevelByName($this->config->get("[game1]sign")["level"]));
						$p->teleport($this->g1sign->getDefaultSpawn());
						$this->status2=0;
						$this->g2time=0;
						break;
	  }
	 }
	 
    }
  }
		
		
		
		
		
		
		
		public function game1(){
			foreach($this->players1 as $p){
			$this->g1time=$this->config->get("queueTime");
			if($this->status1 === 1){
				switch($this->g1time){
						
						case 5:
						$p->sendTip("Game starts in {$this->time1} seconds!");
						break;
						
						case 60:
						$p->sendMessage("Game starts in 1 minute!");
						break;
						
						case 0:
						$p->sendMessage("Game has started!");
						$pl = $this->players1;
						$vip = array_rand($pl,1);
	                    $vip->sendMessage("You are the VIP!");
						$vip->teleport($this->g1p1);
						$p->teleport($this->g1p2);
						$this->g1time=0;
						$this->g1time=$this->config->get("playTime");
						$this->status1=2;
						break;
					}
				}
					
					
				
			if($this->status1 === 2){
			switch($this->g1time){
			
				case 5:
						$p->sendTip("Game ends in {$this->g1time} seconds!");
						break;
						
						case 60:
						$p->sendMessage("Game ends in 1 minute!");
						break;
						
						case 120:
						$p->sendMessage("Game ends in 2 minutes");
						break;
						
						case 180:
						$p->sendMessage("Game ends in 3 minutes!");
						break;
						
						case 0:
						$p->sendMessage("Game has ended!");
						$p->setLevel($this->getServer()->getLevelByName($this->config->get("[game1]sign")["level"]));
						$p->teleport($this->g1sign->getDefaultSpawn());
						$this->status1=0;
						$this->g1time=0;
						break;
	  }
	 }
	 
    }
  }
  
}
